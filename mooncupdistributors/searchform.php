<?php
/**
 * Mooncup Main template for displaying custom search
 *
 * @package WordPress
 * @subpackage Mooncup Main
 * @since Mooncup Main 1.0
 */
?>

<form action="<?php echo home_url(); ?>" method="get" class="searchbox">
	<label for="search">Search in <?php echo home_url( '/' ); ?></label>
	<input type="text" placeholder="Search" name="s" id="search" value="<?php echo esc_attr( get_search_query() ); ?>" />
	 <?php if(  get_current_blog_id()==3 ):?>
    
    <input type="hidden" name="post_type[]" value="using-mooncup" />
<input type="hidden" name="post_type[]" value="testimonial" />
    <?php else:?>
    <input type="hidden" name="lang" value="en">
    <?php endif;?>
</form>