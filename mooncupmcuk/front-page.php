<?php
/**
 * Mooncup Main template for displaying the Front-Page
 *
 * @package WordPress
 * @subpackage Mooncup Main
 * @since Mooncup Main 1.0
 */
 get_header();
 ?>



<section class="page-content primary" role="main">
	<div class="splash">
		<?php
			echo do_shortcode('[rev_slider alias="home-splash-'.ICL_LANGUAGE_CODE.'"]');
		?>
	</div>
	<div class="container_full benefit-container">
		<div class="container_boxed benefit-group">
		<?php

			// check if the repeater field has rows of data
			if( have_rows('home_benefit') ):?>


			    <?php while ( have_rows('home_benefit') ) : the_row();?>

				<div class="home-benefit-item center">
					<div class = "home-benefit__image">
					    <img src="<?php the_sub_field('home_benefit_image');?>" alt="Mooncup benefit Icon">
					</div>

					<div class="home-benefit__intro">
						<?php
							the_sub_field('home_benefit_intro');
						?>
					</div>

					<div class="home-benefit__full">
						<?php
							the_sub_field('home_benefit_content');
						?>
					</div>
				</div>

			    <?php endwhile;?>
		</div>
		<div class="benefit-expand-trigger center grey-bg">
			<?php _e('Tell me more','mooncupmain'); ?> <i class="fa fa-lg fa-angle-down blue"></i>
		</div>

			<?php

			else :

			    // no rows found

			endif;

			?>
	</div>

<?php
// check if the flexible content field has rows of data
if( have_rows('home_layout') ):?>


		<?php
	     // loop through the rows of data
	    while ( have_rows('home_layout') ) : the_row();?>

	        <?php if( get_row_layout() == 'content_area' ):?>

	        	<article class="container_full content_band">
	        		<div class="container_boxed--narrow">
	        	<?php
				$trans_what = get_sub_field('content_block');
	        	_e($trans_what,'mooncupmain');

				?>
	        		</div>
	        	</article>

	        <?php elseif( get_row_layout() == 'splash_and_image_content' ):?>
	        <article class="container_full splash-content-block">
	        	<div class = "splash-image-narrow image_fullwidth" style="background-image:url('<?php the_sub_field('image_area'); ?>');">
		        	<div class="splash-content-overlay center text-reverse">
		        		<div class="container_boxed--narrow content_band">
				        	<?php
				        	the_sub_field('content_overlay');
				        	?>
				        	<a href="<?php the_sub_field('button_url');?>" class="btn btn-primary">
				        		<?php
				        		the_sub_field('button_text');
				        		?>
				        	</a>
			        	</div>
		        	</div>
		        </div>
		    </article>

		    <?php elseif( get_row_layout() == 'content_area_fullwidth' ):?>
			<div class="container_full center" >
				<?php
				   the_sub_field('general_content');
				?>
			</div>

			<?php endif;?>

			<?php
		    endwhile;

			else :
			    echo 'no layouts found';
			endif;
			?>
	
	<div class="container_full">
		<?php

		// check if the repeater field has rows of data
		if( have_rows('image_grid_general') ):?>


		    <?php while ( have_rows('image_grid_general') ) : the_row();?>

			<div class="image-grid-block">
			    <a href="<?php the_sub_field('tile_link_destination');?>" class="">
				    <div class = "image-grid-item" style="background:linear-gradient(rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0.2)), url('<?php the_sub_field('grid_image'); ?>');">
					    <div class="grid-content-container center text-reverse">
					        <div class="grid-content">
						        <?php
						        the_sub_field('tile_text');
						        ?>
						    </div>
					    </div>
					</div>
				</a>
			</div>

		    <?php endwhile;?>

			<?php

			else :

			    // no rows found

			endif;

			?>
	</div>

	<article class="container_full home-blog-post text-reverse center" style="background:linear-gradient(rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0.2)), url('<?php the_field('blog_image'); ?>');">
		<div class="container_boxed--narrow">
			<?php

			$post_object = get_field('blog_feature');

			if( $post_object ):

				// override $post
				$post = $post_object;
				setup_postdata( $post );

				?>
			    <div>
			    	<h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
			    	<p><?php the_excerpt(); ?></p>
			    	<a class="caps small" href="<?php the_permalink(); ?>"><?php _e('Read more','mooncupmain'); ?></a>
			    </div>
			    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
			<?php endif; ?>
		</div>
	</article>


</section>

<?php get_footer(); ?>
